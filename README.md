# focal-projectmanagement-missing-packages

Tracking repository for packages which are not yet packaged for Ubuntu Touch based on Ubuntu 20.04. Serves no other purpose, contains no code, and will be archived when https://gitlab.com/groups/ubports/-/epics/10 is completed.

This repository should *only* be used for packages which are not yet on GitLab. Once packages are here on GitLab, issues should be filed on their repositories.

Every issue in this repository should be added to https://gitlab.com/groups/ubports/-/epics/10 or one of its ancestor Epics as needed.
